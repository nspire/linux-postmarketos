// SPDX-License-Identifier: GPL-2.0
#include "qcom-msm8974pro.dtsi"
#include "qcom-pma8084.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/pinctrl/qcom,pmic-gpio.h>
#include <dt-bindings/leds/common.h>
/ {
	model = "Samsung Galaxy S5";
	compatible = "samsung,klte", "qcom,msm8974";

	aliases {
		serial0 = &blsp1_uart1;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

	smd {
		rpm {
			rpm_requests {
				pma8084-regulators {
					status = "okay";

					s1 {
						regulator-min-microvolt = <675000>;
						regulator-max-microvolt = <1050000>;
					};

					s2 {
						regulator-min-microvolt = <500000>;
						regulator-max-microvolt = <1050000>;
					};

					s3 {
						regulator-min-microvolt = <1300000>;
						regulator-max-microvolt = <1300000>;
					};

					s4 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					s5 {
						regulator-min-microvolt = <2150000>;
						regulator-max-microvolt = <2150000>;
					};

					s6 {
						regulator-min-microvolt = <1050000>;
						regulator-max-microvolt = <1050000>;
					};

					l1 {
						regulator-min-microvolt = <1225000>;
						regulator-max-microvolt = <1225000>;
					};

					l2 {
						regulator-min-microvolt = <1200000>;
						regulator-max-microvolt = <1200000>;
					};

					l3 {
						regulator-min-microvolt = <1050000>;
						regulator-max-microvolt = <1200000>;
					};

					l4 {
						regulator-min-microvolt = <1200000>;
						regulator-max-microvolt = <1225000>;
					};

					l5 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					l6 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					l7 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					l8 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					l9 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <2950000>;
					};

					l10 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <2950000>;
					};

					l11 {
						regulator-min-microvolt = <1300000>;
						regulator-max-microvolt = <1300000>;
					};

					l12 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					l13 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <2950000>;
					};

					l14 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					l15 {
						regulator-min-microvolt = <2050000>;
						regulator-max-microvolt = <2050000>;
					};

					l16 {
						regulator-min-microvolt = <2700000>;
						regulator-max-microvolt = <2700000>;
					};

					l17 {
						regulator-min-microvolt = <2850000>;
						regulator-max-microvolt = <2850000>;
					};

					l18 {
						regulator-min-microvolt = <2850000>;
						regulator-max-microvolt = <2850000>;
					};

					l19 {
						regulator-min-microvolt = <2900000>;
						regulator-max-microvolt = <3300000>;
					};

					l20 {
						regulator-min-microvolt = <2950000>;
						regulator-max-microvolt = <2950000>;

						regulator-allow-set-load;
						regulator-system-load = <200000>;
					};

					l21 {
						regulator-min-microvolt = <2950000>;
						regulator-max-microvolt = <2950000>;
					};

					l22 {
						regulator-min-microvolt = <3000000>;
						regulator-max-microvolt = <3300000>;
					};

					l23 {
						regulator-min-microvolt = <3000000>;
						regulator-max-microvolt = <3000000>;
					};

					l24 {
						regulator-min-microvolt = <3075000>;
						regulator-max-microvolt = <3075000>;
					};

					l25 {
						regulator-min-microvolt = <2100000>;
						regulator-max-microvolt = <2100000>;
					};

					l26 {
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <2050000>;
					};

					l27 {
						regulator-min-microvolt = <1000000>;
						regulator-max-microvolt = <1225000>;
					};
				};
			};
		};
	};
	i2c-gpio-touchkey {
		compatible = "i2c-gpio";
		#address-cells = <1>;
		#size-cells = <0>;
		sda-gpios = <&msmgpio 95 (GPIO_ACTIVE_HIGH | GPIO_OPEN_DRAIN)>;
		scl-gpios = <&msmgpio 96 (GPIO_ACTIVE_HIGH | GPIO_OPEN_DRAIN)>;
		pinctrl-names = "default";
		pinctrl-0 = <&i2c_touchkey_pins>;

		cypress_touchkey@20 {
			compatible = "cypress,tm2-touchkey";
			reg = <0x20>;

			interrupt-parent = <&pma8084_gpios>;
			interrupts = <6 IRQ_TYPE_EDGE_FALLING>;
			pinctrl-names = "default";
			pinctrl-0 = <&touchkey_pin>;

			vcc-supply = <&max77826_ldo15>;
			vdd-supply = <&pma8084_l19>;

			linux,keycodes = <KEY_APPSELECT KEY_BACK>;
		};
	};

	i2c-gpio-led {
		compatible = "i2c-gpio";
		#address-cells = <1>;
		#size-cells = <0>;
		scl-gpios = <&msmgpio 121 (GPIO_ACTIVE_HIGH | GPIO_OPEN_DRAIN)>;
		sda-gpios = <&msmgpio 120 (GPIO_ACTIVE_HIGH | GPIO_OPEN_DRAIN)>;
		pinctrl-names = "default";
		pinctrl-0 = <&i2c_led_gpioex_pins>;

		i2c-gpio,delay-us = <2>;

		gpio_expander: gpio@20 {
			compatible = "nxp,pcal6416";
			reg = <0x20>;

			gpio-controller;
			#gpio-cells = <2>;

			vcc-supply = <&pma8084_s4>;

			pinctrl-names = "default";
			pinctrl-0 = <&gpioexpander_pin>;

			reset-gpios = <&msmgpio 145 GPIO_ACTIVE_LOW>;
		};

		led-controller@30 {
			reg = <0x30>;
			compatible = "panasonic,an30259a";

			#address-cells = <1>;
			#size-cells = <0>;

			led@1 {
				reg = <1>;
				function = LED_FUNCTION_STATUS;
				color = <LED_COLOR_ID_RED>;
			};

			led@2 {
				reg = <2>;
				function = LED_FUNCTION_STATUS;
				color = <LED_COLOR_ID_GREEN>;
			};

			led@3 {
				reg = <3>;
				function = LED_FUNCTION_STATUS;
				color = <LED_COLOR_ID_BLUE>;
			};
		};
	};

	vreg_wlan: wlan-regulator {
		compatible = "regulator-fixed";

		regulator-name = "wl-reg";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;

		gpio = <&gpio_expander 8 GPIO_ACTIVE_HIGH>;
		enable-active-high;

};

&soc {
	serial@f991e000 {
		status = "ok";
	};

	gpio-keys {
		compatible = "gpio-keys";
		input-name = "gpio-keys";

		pinctrl-names = "default";
		pinctrl-0 = <&gpio_keys_pin_a>;

		volume-down {
			label = "volume_down";
			gpios = <&pma8084_gpios 2 GPIO_ACTIVE_LOW>;
			linux,input-type = <1>;
			linux,code = <KEY_VOLUMEDOWN>;
			debounce-interval = <15>;
		};

		home-key {
			label = "home_key";
			gpios = <&pma8084_gpios 3 GPIO_ACTIVE_LOW>;
			linux,input-type = <1>;
			linux,code = <KEY_HOMEPAGE>;
			wakeup-source;
			debounce-interval = <15>;
		};

		volume-up {
			label = "volume_up";
			gpios = <&pma8084_gpios 5 GPIO_ACTIVE_LOW>;
			linux,input-type = <1>;
			linux,code = <KEY_VOLUMEUP>;
			debounce-interval = <15>;
		};
	};

	pinctrl@fd510000 {
		sdhc1_pin_a: sdhc1-pin-active {
			clk {
				pins = "sdc1_clk";
				drive-strength = <4>;
				bias-disable;
			};

			cmd-data {
				pins = "sdc1_cmd", "sdc1_data";
				drive-strength = <4>;
				bias-pull-up;
			};
		};
	};

	sdhci@f9824900 {
		status = "ok";

		vmmc-supply = <&pma8084_l20>;
		vqmmc-supply = <&pma8084_s4>;

		bus-width = <8>;
		non-removable;

		pinctrl-names = "default";
		pinctrl-0 = <&sdhc1_pin_a>;
	};
		sdhc2_pin_a: sdhc2-pin-active {
			clk {
				pins = "sdc2_clk";
				drive-strength = <6>;
				bias-disable;
			};

			cmd-data {
				pins = "sdc2_cmd", "sdc2_data";
				drive-strength = <6>;
				bias-pull-up;
			};
		};

		i2c2_pins: i2c2 {
			mux {
				pins = "gpio6", "gpio7";
				function = "blsp_i2c2";

				drive-strength = <2>;
				bias-disable;
			};
		};

		i2c6_pins: i2c6 {
			mux {
				pins = "gpio29", "gpio30";
				function = "blsp_i2c6";

				drive-strength = <2>;
				bias-disable;
			};
		};

		i2c_touchkey_pins: i2c-touchkey {
			mux {
				pins = "gpio95", "gpio96";
				function = "gpio";
				input-enable;
				bias-pull-up;
			};
		};

		i2c_led_gpioex_pins: i2c-led-gpioex {
			mux {
				pins = "gpio120", "gpio121";
				function = "gpio";
				input-enable;
				bias-pull-down;
			};
		};

		panel_pin: panel {
			te {
				pins = "gpio12";
				function = "mdp_vsync";

				drive-strength = <2>;
				bias-disable;
			};
		};

		wifi_int_pin: wifi {
			int {
				pins = "gpio92";
				function = "gpio";

				input-enable;
				bias-pull-down;
			};
		};

		gpioexpander_pin: gpioex {
			res {
				pins = "gpio145";
				function = "gpio";

				bias-pull-up;
				drive-strength = <2>;
			};

	usb@f9a55000 {
		status = "ok";

		phys = <&usb_hs1_phy>;
		phy-select = <&tcsr 0xb000 0>;
		/*extcon = <&smbb>, <&usb_id>;*/
		/*vbus-supply = <&chg_otg>;*/

		hnp-disable;
		srp-disable;
		adp-disable;

		ulpi {
			phy@a {
				status = "ok";

				v1p8-supply = <&pma8084_l6>;
				v3p3-supply = <&pma8084_l24>;

				/*extcon = <&smbb>;*/
				qcom,init-seq = /bits/ 8 <0x1 0x64>;
			};
		};
	};
};

&spmi_bus {
	pma8084@0 {
		gpios@c000 {
			gpio_keys_pin_a: gpio-keys-active {
				pins = "gpio2", "gpio3", "gpio5";
				function = "normal";

				bias-pull-up;
				power-source = <PMA8084_GPIO_S4>;
			};
		};

		/*TODO: remove pm8941 dependency in qcom-msm8974.dtsi*/
		pm8941_gpios: gpios@c000 {
			boost_bypass_n_pin: boost-bypass {
			};
		};
	};
};
